::cmd.::
    run, %comspec% /k cd C:\Users\mstrag\Documents\trf-infrastructure\deployment\docker-compose\local
Return

::r.::
    run, %comspec% /k cd C:\Users\mstrag\Documents\trf-infrastructure\deployment\docker-compose\local
	Sleep, 2000  ; 1 second
    Send, docker-compose pull & docker-compose up -d
Return

::sr.::
    Send, docker-compose pull & docker-compose up -d
Return

::k.::
    Send, docker-compose down
Return

::ds.::
   Send {Raw}docker stats --format "table{{.Name}}\t{{.CPUPerc}}\t{{.MemUsage}}\t{{.MemPerc}}"
Return

::da.::
   Send, docker ps -a
Return

::mail.::
   Send, http://10.132.0.2:8025
Return

::log.::
   Send, docker logs local_trf-api-versions_1
Return

::sip.::
   Send, 10.132.0.3
Return

::cl.::
   Send, docker volume prune
Return

::p.::
   Send, docker-compose pull
Return