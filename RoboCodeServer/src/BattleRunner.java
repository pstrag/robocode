import robocode.control.*;
import robocode.control.events.*;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

//
// Application that demonstrates how to run two sample robots in Robocode using the
// RobocodeEngine from the robocode.control package.
//
// @author Flemming N. Larsen
//
public class BattleRunner {
    static int roundNo = 0;
    public static void main(String[] args) {

        // Disable log messages from Robocode
        RobocodeEngine.setLogMessagesEnabled(false);

        // Create the RobocodeEngine
        //   RobocodeEngine engine = new RobocodeEngine(); // Run from current working directory
        RobocodeEngine engine = new RobocodeEngine(new java.io.File("C:/Robocode")); // Run from C:/Robocode

        // Add our own battle listener to the RobocodeEngine
        engine.addBattleListener(new BattleObserver());

        // Show the Robocode battle view
        engine.setVisible(true);

        // Setup the battle specification

        int numberOfRounds = 1;
        BattlefieldSpecification battlefield = new BattlefieldSpecification(700, 700); // 800x600
        RobotSpecification[] selectedRobots;
        BattleSpecification battleSpec;

        // Run our specified battle and let it run till it is over
        while(true) {
            selectedRobots = engine.getLocalRepository();
            battleSpec = new BattleSpecification(numberOfRounds, battlefield, selectedRobots);
            roundNo ++;
            engine.runBattle(battleSpec, true); // waits till the battle finishes
        }
        // Cleanup our RobocodeEngine
      //  engine.close();

        // Make sure that the Java VM is shut down properly
      //  System.exit(0);
    }
}

//
// Our private battle listener for handling the battle event we are interested in.
//
class BattleObserver extends BattleAdaptor {
    BattleRunner br = new BattleRunner();
    // Called when the battle is completed successfully with battle results
    public void onBattleCompleted(BattleCompletedEvent e) {
        System.out.println("-- Battle has completed --");
        // Print out the sorted results with the robot names
        System.out.println("Battle results for battle " + br.roundNo);
        for (robocode.BattleResults result : e.getSortedResults()) {
            System.out.println("  " + result.getTeamLeaderName() + ": " + result.getScore());

            try {
                BufferedWriter writer = new BufferedWriter(new FileWriter("C://result.txt", true));
                writer.write(result.getTeamLeaderName()+"; " + result.getScore()+";");
                writer.newLine();
                writer.close();
            } catch(IOException a){
                a.printStackTrace();
            }
        }
    }

    // Called when the game sends out an information message during the battle
    public void onBattleMessage(BattleMessageEvent e) {
        System.out.println("Msg> " + e.getMessage());
    }

    public void onRoundEnded(RoundEndedEvent event) {
        System.out.println("to juz koniec");

    }
    // Called when the game sends out an error message during the battle
    public void onBattleError(BattleErrorEvent e) {
        System.out.println("Err> " + e.getError());
    }
}