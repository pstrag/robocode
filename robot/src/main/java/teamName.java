import robocode.*;
import robocode.Robot;

import java.awt.*;


//API https://robocode.sourceforge.io/docs/robocode/
// robocode -> robot
public class teamName extends Robot {
    public void run() {
        Color tankColor = Color.blue;
        setColors(tankColor, tankColor, tankColor);
        while (true) {
            ahead(400);
            turnGunRight(400);
            back(400);
            turnGunRight(400);
        }
    }
    public void onScannedRobot(ScannedRobotEvent e) {
        fire(1);
    }

}

